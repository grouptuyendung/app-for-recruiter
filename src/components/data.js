// import callApi from './apiCaller';
// var listCV = [];
//     callApi('api/read.php', 'GET',  {
//       'Content-Type': 'application/json',
//       'X-Requested-With' : 'XMLHttpRequest'
//   }).then((responseJson) => {
//     console.log(responseJson);
//     listCV =  responseJson.body;
//   });
//   console.log(listCV);
export default
[
    {
        id: '1',
        name: "Trần Nhật Nam",
        job: "React native fresher",
        school: "Hutech",
        avatar: require('../images/img-nhatnam.jpg'),
        birthday: "19/08/1999",
        email: 'nhatnam99132@gmail.com',
        phone: '0917650658',
        address: 'Thủ đức',
        careerObjectives: 'Trở thành chuyên gia trong lĩnh vực mobile - Công việc phù hợp và phát triển bản thân',
        personalSkills: 'Kỹ năng làm việc nhóm, Kỹ năng giao tiếp, nhiệt tình, trung thực, trách nhiệm',
        qualifications: 'Tiếng anh b1'
      },
      {
        id: '2',
        name: "Trần Hoàng Hải Long",
        job: "ASP.NET MVC",
        school: "Hutech",
        avatar: require('../images/img-long.jpg'),
        birthday: "01/01/199",
        email: 'abc@gmail.com',
        phone: '0912345678',
        address: '123 điện biên phủ',
        careerObjectives: 'Trở thành chuyên gia trong lĩnh vực web - Công việc phù hợp và phát triển bản thân',
        personalSkills: '1 năm làm việc với web ,Kỹ năng làm việc nhóm, Kỹ năng giao tiếp, nhiệt tình, trung thực, trách nhiệm',
        qualifications: 'Toeic 600,'
      },
      {
        id: '3',
        name: "Lê Huy Hải Nam",
        job: "Backend fresher",
        school: "Hutech",
        avatar: require('../images/img-hainam.jpg'),
        birthday: "01/01/2000",
        email: 'abc@gmail.com',
        phone: '0912345678',
        address: '123 điện biên phủ',
        careerObjectives: 'Trở thành chuyên gia trong lĩnh vực PHP mySQL - Công việc phù hợp và phát triển bản thân',
        personalSkills: '1 năm làm việc trong fpt software, Kỹ năng làm việc nhóm, Kỹ năng giao tiếp, nhiệt tình, trung thực, trách nhiệm',
        qualifications: 'Toeic 600'
      },
      {
        id: '4',
        name: "Phạm Hoàng Trung Nghĩa",
        job: "Computer Sciene",
        school: "Hutech",
        avatar: require('../images/img-nghia.jpg'),
        birthday: "01/01/2000",
        email: 'abc@gmail.com',
        phone: '0912345678',
        address: '123 điện biên phủ',
        careerObjectives: 'Trở thành chuyên gia trong lĩnh vực AI - Công việc phù hợp và phát triển bản thân',
        personalSkills: 'Từng đạt giải nhất trong cuộc thi nghiên cứu khoa học ở trường, Kỹ năng làm việc nhóm, Kỹ năng giao tiếp, nhiệt tình, trung thực, trách nhiệm',
        qualifications: 'Toeic 900'
      },
      {
        id: '5',
        name: "Lê Công Huy",
        job: "Java android",
        school: "Hutech",
        avatar: require('../images/img-huy.jpg'),
        birthday: "01/01/2000",
        email: 'abc@gmail.com',
        phone: '0912345678',
        address: '123 điện biên phủ',
        careerObjectives: 'Trở thành chuyên gia trong lĩnh vực mobile - Công việc phù hợp và phát triển bản thân',
        personalSkills: 'Kỹ năng làm việc nhóm, Kỹ năng giao tiếp, nhiệt tình, trung thực, trách nhiệm',
        qualifications: 'Toeic 900'
      },
      {
        id: '6',
        name: "Phạm Minh Trung",
        job: "Web developer",
        school: "Hutech",
        avatar: require('../images/img-trung.jpg'),
        birthday: "01/01/2000",
        email: 'abc@gmail.com',
        phone: '0912345678',
        address: '123 điện biên phủ',
        careerObjectives: 'Trở thành chuyên gia trong lĩnh vực web - Công việc phù hợp và phát triển bản thân',
        personalSkills: 'Thành thạo html css javascript, 1 năm làm việc với reactJS, Kỹ năng làm việc nhóm, Kỹ năng giao tiếp, nhiệt tình, trung thực, trách nhiệm',
        qualifications: 'Toeic 900'
      },
      {
        id: '7',
        name: "Hoàng Hữu Thiên Bảo",
        job: "PHP",
        school: "Hutech",
        avatar: require('../images/img-bao.jpg'),
        birthday: "01/01/2000",
        email: 'abc@gmail.com',
        phone: '0912345678',
        address: '123 điện biên phủ',
        careerObjectives: 'Trở thành chuyên gia trong lĩnh vực mobile - Công việc phù hợp và phát triển bản thân',
        personalSkills: 'Kỹ năng làm việc nhóm, Kỹ năng giao tiếp, nhiệt tình, trung thực, trách nhiệm',
        qualifications: 'Toeic 900'
      },
]
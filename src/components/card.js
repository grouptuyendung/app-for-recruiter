import React, {useEffect, useState} from 'react';
import {View, Button, RefreshControl, ActivityIndicator, FlatList, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import callApi from '../components/apiCaller';


function ViewCard() {
  const [listCard, setListCard] = useState();
  const [isLoading, setLoading] = useState(true);
  const [fetching, setFetching] = useState(false);
  const [idUser, setIdUser] =  useState();
  
  const getCV =  async () => {
    const a = await AsyncStorage.getItem('idUser');
    console.log("Hello" +a);

    //await setUser(id);
    //console.log(idUser);
    await callApi('api/carecv', 'POST',  {
      idRecruiter: a,
  }, {
      'Content-Type': 'application/json',
      'X-Requested-With' : 'XMLHttpRequest'
  }).then(async (responseJson) => {
    //console.log(responseJson.data);
    
      await setListCard(responseJson.data.data);
     
    
    setLoading(false);
    setFetching(false);
  }).catch(err => {
    console.log('id user: '+a);
    console.log(err);
  });
  
  }
  useEffect(() => {
    
    
   getCV();
   console.log('abc');
   
  }, []);
  const onRefresh = () => {
    setFetching(true);
    getCV();
  };
    const navigation = useNavigation();
    return(
      
      <View style={styles.container}>
      
      {isLoading ? <ActivityIndicator/> : (
        <View style={styles.listCard}>
          {/* Dùng Flatlist show danh sách product */}
          <FlatList
            data={listCard}
            refreshControl={
              <RefreshControl
                refreshing={fetching}
                onRefresh={() => onRefresh()}
              />
            }
            ListEmptyComponent={
            <View>
              <Text style={styles.title}>Chưa có ứng viên trong danh sách</Text>
              <View style={styles.loginButton}>
              <Button title="Làm mới" onPress={() => onRefresh()}/>
              </View>
            </View>
            }
            keyExtractor={(item, index) => String(index)}
            renderItem={({ item }) => (
              <View style={styles.cardView}>
                <TouchableOpacity style={styles.cardTouch} onPress={() => navigation.navigate('Detail', {cardDetail: item})} >
                  <View style={styles.cardGroup}>
                    <View style={styles.cardImage} >
                      <Image source={require('../images/avatar.png')} style={styles.avatarImage} />
                    </View>
                    <View style={styles.cardText}>
                      <Text style={styles.cardName}>{item.full_name}</Text>
                      <Text style={styles.cardPrice}>{item.fields}</Text>
                      <Text style={styles.cardPrice}>{item.level_desired}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          />
        </View>
      )}
      </View>
    );
  }


export default ViewCard;
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
    },
    headerButton: {
      margin: 10,
      backgroundColor: '#333',
    },
    cardGroup: {
      borderWidth: 1,
      flexDirection: "row",
      margin: 15,
      padding: 5,
      backgroundColor: '#fff',
      borderRadius: 10,
      shadowColor: '#fff',
      shadowOffset: {width: 0, height: 2},
      shadowOpacity: 0.2,
      shadowRadius: 4,
    },
    cardText: {
      flex: 3,
    },
    avatarImage: {
      width: 100,
      height: 120,
      margin: 5,
      
    },
    cardImage: {
      flex: 2,
    },
    cardImageHorizontal: {
      alignItems:"center"
    }, 
    cardName: {
      fontWeight: "bold",
    },
    cardGroupHorizontal: {
      borderWidth: 1,
      margin: 5,
      padding: 5,
      width: 200,
      backgroundColor: '#fff',
      borderRadius: 10,
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 2},
      shadowOpacity: 0.25,
      shadowRadius: 4,
    },
    cardTextHorizontal: {
      alignItems: "center"
    },
    cardView: {
      backgroundColor: '#ddd'
    },
    title: {
      fontSize: 20,
      textAlign: 'center',
      color: '#444',
    },
    loginButton: {
      backgroundColor: '#3897f1',
      borderRadius: 15,
      margin: 40,
  },
  listCard: {
    flex: 1
}
  });
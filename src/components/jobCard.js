import React from 'react';
import {View, FlatList, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import { useNavigation } from '@react-navigation/native';
const listCard = [
    {
      id: '1',
      name: "Công ty FPT software",
      infoJob: "Cần tìm 10 nhân viên fresher lập trình android",
      avatar: require('../images/FPT_Software.png'),
    },
    {
        id: '2',
        name: "Công ty HUTECH",
        infoJob: "Cần tìm 10 nhân viên fresher lập trình web ASP.NET",
        avatar: require('../images/hutech.png'),
      },
      {
        id: '3',
        name: "Công ty VNG",
        infoJob: "Cần tìm 10 nhân viên fresher lập trình ReactJS",
        avatar: require('../images/vng-logo.png'),
      },
      {
        id: '4',
        name: "Công ty phần mềm Quang Trung",
        infoJob: "Cần tìm 10 nhân viên fresher lập trình Java",
        avatar: require('../images/quangtrung.png'),
      },
      {
        id: '5',
        name: "Công ty Viettel",
        infoJob: "Cần tìm 10 nhân viên lập trình mobile developer",
        avatar: require('../images/Logo_Viettel.png'),
      },
      {
        id: '6',
        name: "Công ty LG",
        infoJob: "Cần tìm 10 nhân viên lập trình fullstack javascript(Node JS/ ReactJS)",
        avatar: require('../images/lg-logo.png'),
      },
      {
        id: '7',
        name: "Công ty KMS",
        infoJob: "Cần tìm 10 nhân viên lập trình fullstack .NET ",
        avatar: require('../images/kms-logo.png'),
      },
      {
        id: '8',
        name: "Công ty GRAB",
        infoJob: "Cần tìm 10 nhân viên lập trình React native",
        avatar: require('../images/grab-logo.png'),
      },
      {
        id: '9',
        name: "Công ty garena",
        infoJob: "Cần tìm 10 nhân viên lập trình .NET",
        avatar: require('../images/garena.png'),
      },
   
  ];

function ViewJobCard() {
    const navigation = useNavigation();
    return(
      <View style={styles.container}>
        <View style={styles.listCard}>
          {/* Dùng Flatlist show danh sách product */}
          <FlatList
            data={listCard}
            keyExtractor={item => item.id}
            renderItem={({ item }) => (
              <View style={styles.cardView}>
                <TouchableOpacity style={styles.cardTouch} >
                  <View style={styles.cardGroup}>
                    <View style={styles.cardImage} >
                      <Image source={item.avatar} style={styles.avatarImage} />
                    </View>
                    <View style={styles.cardText}>
                      <Text style={styles.cardName}>{item.name}</Text>
                      <Text style={styles.cardPrice}>{item.infoJob}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          />
        </View>
      </View>
    );
  }

export const ViewJobCardHorizontal = () => {
  const navigation = useNavigation();
  return(
    <View style={styles.container}>
        <View style={styles.listCard}>
          {/* Dùng Flatlist show danh sách product */}
          <FlatList
            horizontal={true}
            data={listCard}
            keyExtractor={item => item.id}
            renderItem={({ item }) => (
              <View style={styles.cardViewHorizontal}>
                <TouchableOpacity>
                  <View style={styles.cardGroupHorizontal}>
                    <View style={styles.cardImageHorizontal} >
                      <Image source={item.avatar} style={styles.avatarImage} />
                    </View>
                    <View style={styles.cardTextHorizontal}>
                      <Text style={styles.cardName}>{item.name}</Text>
                      <Text style={styles.cardPrice}>{item.infoJob}</Text>

                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          />
        </View>
      </View>
  );
}
export default ViewJobCard;
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
    },
    headerButton: {
      margin: 10,
      backgroundColor: '#333',
    },
    cardGroup: {
      borderWidth: 1,
      flexDirection: "row",
      margin: 15,
      padding: 5,
      backgroundColor: '#fff',
      borderRadius: 10,
      shadowColor: '#fff',
      shadowOffset: {width: 0, height: 2},
      shadowOpacity: 0.2,
      shadowRadius: 4,
    },
    cardText: {
      flex: 3,
    },
    avatarImage: {
      width: 100,
      height: 120,
      margin: 5,
      resizeMode: "stretch"
      
    },
    cardImage: {
      flex: 2,
    },
    cardImageHorizontal: {
      alignItems:"center"
    }, 
    cardName: {
      fontWeight: "bold",
    },
    cardGroupHorizontal: {
      borderWidth: 1,
      margin: 5,
      padding: 5,
      width: 200,
      backgroundColor: '#fff',
      borderRadius: 10,
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 2},
      shadowOpacity: 0.25,
      shadowRadius: 4,
    },
    cardTextHorizontal: {
      alignItems: "center"
    },
    cardView: {
      backgroundColor: '#ddd'
    },
  });
import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, TouchableOpacity, StatusBar } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';
import callApi from '../components/apiCaller';
function ProfileItem ({icon, name}) {
  return(
  <View style={styles.itemContainer}>
    <MaterialCommunityIcons name={icon} size={26} color="#1e1e1e" /> 
      <Text style={[styles.itemText, {marginLeft: icon ? 20 : 0}]}>{name}</Text>
    <FontAwesome5 name="angle-right" size={26} color="#1e1e1e" />
  </View> 
)
}

export default function Profile ({navigation}) {
  const [getUser, setUser] = useState({});
   

  useEffect(() => {
    async function getUser() {
      const userToken = await AsyncStorage.getItem('TokenUngVien');
      console.log(userToken);
      await callApi('api/candidate/user-profile', 'GET', {
    }, {
      'Authorization': 'Bearer ' + userToken
    }).then(async (response) => {
        console.log(response.data);
        await setUser(response.data);
        
    }).catch(error => {
      if(error.response.status == 401) {
         AsyncStorage.removeItem('TokenUngVien');
    
        navigation.replace("Login");
        
      }
      else if(error.response.status == 422) {
         AsyncStorage.removeItem('TokenUngVien');
        navigation.replace("Login");
      }
      else {
          Alert.alert('Thông báo', 'Lỗi kết nối đến máy chủ! Vui lòng thử lại sau' + error, [{
              text: 'OK',
              onPress: () => {navigation.replace("Login");  AsyncStorage.removeItem('TokenUngVien');}
          }]);
      }
      
      console.log(error);
  });
    }
    getUser();
  }, [])
  
  const logOut = async () => {
    const userToken =  await AsyncStorage.getItem('TokenUngVien');
    console.log(userToken);
    await callApi('api/candidate/logout', 'POST', {
  }, {
    'Authorization': 'Bearer ' + userToken
  }).then(async (response) => {
      console.log(response.data);
      await AsyncStorage.removeItem('TokenUngVien');
      
      
  });
    navigation.replace("Login");
  }
    return(
      <View style={styles.screenContainer}>
      <StatusBar barStyle="light-content" />
      {/*  */}
      <View style={styles.headerContainer}>
        {/*  */}
        <View style={styles.cartContainer}>
          <View style={styles.cartIcon} />
        </View>
        {/*  */}
        <Text style={styles.headerText}>Hồ sơ</Text>
        {/*  */}
        <View style={styles.cartContainer}>
          <FontAwesome5
            name="address-card"
            size={HEADER_ICON_SIZE}
            color="#fff"
          />
        </View>
      </View>
      {/*  */}
      <View style={styles.bodyContainer}>
      <TouchableOpacity style={styles.userContainer} onPress={() => {
          navigation.navigate('MyProfile', {
          name: getUser.name,
          email: getUser.email,
          address: getUser.address,
          phoneNumber: getUser.phoneNumber,
        });
        }} >
          <View style={styles.avatarContainer}>
            <MaterialIcons name="person" size={26} color="#fff" />
          </View>
          <View style={styles.textContainer}>
            <Text style={styles.nameText}>{getUser.name}</Text>
            <Text style={styles.gmailText}>{getUser.email}</Text>
          </View>
          <FontAwesome5 name="angle-right" size={26} color="#1e88e5" />
        </TouchableOpacity>
        
        {/*  */}
        <View style={styles.divider} />
        <TouchableOpacity>
          <ProfileItem icon="settings" name="Cài đặt" />
        </TouchableOpacity>
        {/*  */}
        <View style={styles.divider} />
        <TouchableOpacity onPress={logOut}>
          <ProfileItem icon="logout" name="Đăng xuất" />
        </TouchableOpacity>
      </View>
    </View>
      );
}
const HEADER_ICON_SIZE = 24;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
  },
  headerContainer: {
    flexDirection: 'row',
    paddingTop: 10,
    backgroundColor: '#1e88e5',
    justifyContent: 'space-between',
    paddingBottom: 12,
  },
  cartContainer: {
    paddingHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cartIcon: {
    width: HEADER_ICON_SIZE,
  },
  headerText: {
    color: '#fff',
    fontSize: 20,
    fontWeight: '500',
  },
  //
  bodyContainer: {
    flex: 1,
    backgroundColor: '#ddd',
  },
  //
  userContainer: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 22,
    alignItems: 'center',
  },
  avatarContainer: {
    width: 50,
    height: 50,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#1e88e5',
  },
  textContainer: {
    flex: 1,
    marginLeft: 20,
  },
  nameText: {
    color: '#1e88e5',
    fontSize: 18,
    fontWeight: '500',
  },
  gmailText: {
    color: '#828282',
  },
  divider: {
    height: 10,
  },
  itemContainer: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 10,
    alignItems: 'center',
  },
  itemText: {
    flex: 1,
    color: '#1e1e1e',
  },
});
import {createContext} from 'react';
// here we can initialise with any value we want.
const UserContext = createContext({}); 
export default UserContext;

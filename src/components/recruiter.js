import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import ViewCard from './card';
import ViewCardNoCare from './cardnocare';
const Tab = createMaterialTopTabNavigator();

function CareCard({ navigation }) {
  return (
    <ViewCard />
  );
}
function NoCareCard({ navigation }) {
  return (
    <ViewCardNoCare />
  );
}

function Recruiter() {
  return (
    <Tab.Navigator tabBarOptions={{
      labelStyle: { fontSize: 14, color: '#fff', fontWeight: 'bold' },
      style: { backgroundColor: '#1e88e5', },
      activeTintColor: '#000',
      indicatorStyle: { backgroundColor: '#FF8000' }
    }}>
      <Tab.Screen
        name="seenCard"
        component={CareCard}
        options={{
          title: "Ứng viên quan tâm",
        }}
      />
      <Tab.Screen
        name="saveCard"
        component={NoCareCard}
        options={{
          title: 'Ứng viên không quan tâm',

        }}
      />
    </Tab.Navigator>
  );
}
export default Recruiter;

import React from 'react';
import {View} from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
const Tab = createMaterialTopTabNavigator();

function SeenCard({ navigation }) {
  return (
    <View>

    </View>
  );
}
function SaveCard({ navigation }) {
  return (
    <View>

    </View>
  );
}

function Job() {
  return (
    <Tab.Navigator tabBarOptions={{
      labelStyle: { fontSize: 14, color: '#fff', fontWeight: 'bold' },
      style: { backgroundColor: '#1e88e5', },
      activeTintColor: '#000',
      indicatorStyle: { backgroundColor: '#FF8000' }
    }}>
      <Tab.Screen
        name="seenCard"
        component={SeenCard}
        options={{
          title: "Việc làm đã xem",
        }}
      />
      <Tab.Screen
        name="saveCard"
        component={SaveCard}
        options={{
          title: 'Việc làm quan tâm',

        }}
      />
    </Tab.Navigator>
  );
}
export default Job;

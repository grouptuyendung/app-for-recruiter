import React from 'react';
import { StyleSheet, Text, ScrollView, View, StatusBar, TouchableOpacity} from 'react-native';
import { ViewJobCardHorizontal } from './jobCard';
import { useNavigation } from '@react-navigation/native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
export default function FindJob() {
  return (
    <ScrollView style={styles.screenContainer}>
      <SearchBar />
      <Text style={styles.label}>Gợi ý việc làm</Text>
      <ViewJobCardHorizontal />
      <Text style={styles.label}>Việc làm được xem nhiều nhất</Text>
      <ViewJobCardHorizontal />
      <Text style={styles.label}>Việc làm mới nhất</Text>
      <ViewJobCardHorizontal />
    </ScrollView>
  );
}
const SearchBar = () => {    
  const navigation = useNavigation();
  return (
      <View>
          <StatusBar barStyle="light-content" />
          {/*  */}
          <View style={styles.headerContainer}>
              <TouchableOpacity onPress={() => navigation.navigate('searchDetail')}>
                  <View style={styles.inputContainer}>
                      <FontAwesome name="search" size={24} color="#969696" />
                      <Text style={styles.inputText}>Tìm việc làm</Text>
                  </View>
              </TouchableOpacity>
              {/*  */}
          </View>
      </View>
  );
}

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    backgroundColor: '#ddd'
  },
  label: {
    fontSize: 18,
    margin: 10,
    fontWeight: 'bold',
    color: '#333'
  },
  inputContainer: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    flex: 1,
    margin: 10,
    padding: 10,
    alignItems: 'center',
    paddingHorizontal: 8,
    borderRadius: 2,
},
headerContainer: {
  flexDirection: "column",
  paddingTop: 4,
  paddingBottom: 4,
  backgroundColor: '#1e88e5',
},
inputText: {
  color: '#969696',
  fontSize: 16,
  marginLeft: 8,
  fontWeight: '500',
  flex: 1
},
});
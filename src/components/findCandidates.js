import React, { useState, useEffect,useContext } from 'react';
import { StyleSheet, ActivityIndicator,SafeAreaView, Text, StatusBar, View, Image, Dimensions} from 'react-native';
import callApi from '../components/apiCaller';
import Swiper from 'react-native-deck-swiper';
// import data from './data';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Transitioning, Transition } from 'react-native-reanimated';
import { ScrollView } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';

const { width } = Dimensions.get('window');



const stackSize = 2;
const colors = {
  red: '#EC2379',
  blue: '#0070FF',
  gray: '#777777',
  white: '#ffffff',
  black: '#000000'
};
const ANIMATION_DURATION = 200;

const transition = (
  <Transition.Sequence>
    <Transition.Out
      type='slide-bottom'
      durationMs={ANIMATION_DURATION}
      interpolation='easeIn'
    />
    <Transition.Together>
      <Transition.In
        type='fade'
        durationMs={ANIMATION_DURATION}
        delayMs={ANIMATION_DURATION / 2}
      />
      <Transition.In
        type='slide-bottom'
        durationMs={ANIMATION_DURATION}
        delayMs={ANIMATION_DURATION / 2}
        interpolation='easeOut'
      />
    </Transition.Together>
  </Transition.Sequence>
);

const swiperRef = React.createRef();
const transitionRef = React.createRef();




export default function FindCandidates() {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [idUser, setUser] =  useState();
   AsyncStorage.getItem('idUser').then(async res => {
    console.log("from isSignedIn : "+res); //res is showing null.
  if (res !== null) {
    console.log(res);
     setUser(res);
  } else {
    console.log(0);
  }
});

  const Care = () => {
    swiperRef.current.swipeRight();
    
    console.log(data.data[index]);
    callApi('api/addcare', 'POST', {
      idCandidate: data.data[index].id,
      idRecruiter: idUser,
      care: 1
  }, {
      'Content-Type': 'application/json',
      'X-Requested-With' : 'XMLHttpRequest'
  }).then(async (response) => {
      await AsyncStorage.removeItem('careCandidate');
      await AsyncStorage.setItem('careCandidate', data.data[index].fields)
      console.log(response.data);
      console.log(data.data[index].fields);   
      console.log(idUser);
  }).catch(error => {
      console.log(error);
      console.log(idUser);
      
  })
  };
  const DontCare = async () => {
    swiperRef.current.swipeLeft();
    callApi('api/adddontcare', 'POST', {
      idCandidate: data.data[index].id,
      idRecruiter: idUser,
      care: 0
  }, {
      'Content-Type': 'application/json',
      'X-Requested-With' : 'XMLHttpRequest'
  }).then(async (response) => {
      console.log(response.data);      
  }).catch(error => {
      console.log(error);
  })
    
  };
  useEffect(() => {
    const getCV =  async () => {
      const a = callApi('api/list', 'GET',  {
        'Content-Type': 'application/json',
        'X-Requested-With' : 'XMLHttpRequest'
    }).then((responseJson) => {
      console.log(responseJson.data);
      setData(responseJson.data);
      setLoading(false);
    }).catch(error => {
      console.log(error);
  });
    
    }
    console.log(data);
   getCV();
    
  }, []);
  const [index, setIndex] = React.useState(0);
  
  const onSwiped = () => {
    transitionRef.current.animateNextTransition();
    setIndex((index + 1) % data.itemCount);
  };
  const Card = ({ card }) => (
    <View style={styles.card}>
      <Image source={require('../images/avatar.png')} style={styles.cardImage}/>
    </View>
  )
  const CardDetails = ({ index }) => (
    <View key={data.data[index].id}>
      <Text style={[styles.text, styles.heading]} numberOfLines={2}>
      <View>
          <Text style={styles.titleInfo}>{data.data[index].full_name}</Text>
        </View>
        <View>
          <Text style={styles.titleInfo}>{data.data[index].birthday}</Text>
        </View>
        <View>
          <Text style={styles.titleInfo}>{data.data[index].type_work}</Text>
        </View>
      </Text>
      <Text style={[styles.text, styles.price]}>{data.data[index].fields}</Text>
      <Text style={styles.text}><Text style={styles.text1}>Lương tối thiểu:</Text> {data.data[index].desired_min_salary}</Text>
      <Text  style={styles.text}><Text style={styles.text1}>Lương tối đa:</Text>{data.data[index].desired_max_salary}</Text>
      <Text  style={styles.text}><Text style={styles.text1}>Cấp bậc mong muốn:</Text> {data.data[index].level_desired}</Text>
      <Text  style={styles.text}><Text style={styles.text1}>Kinh nghiệm làm việc:</Text> {data.data[index].years_experience}</Text>
      <Text  style={styles.text}><Text style={styles.text1}>Địa chỉ:</Text> {data.data[index].address}{"\n"}</Text>
    </View>
  );
  return (
    //  <Text>Hello</Text>
    
    <SafeAreaView style={styles.container}>

      <MaterialCommunityIcons
        name='crop-square'
        size={width}
        color={colors.blue}
        style={{
          opacity: 0.05,
          transform: [{ rotate: '45deg' }, { scale: 1.6 }],
          position: 'absolute',
          left: -15,
          top: 30
        }}
      />
      <StatusBar hidden={true} />
      {isLoading ? <ActivityIndicator/> : (
      <View style={styles.swiperContainer}>
        <Swiper
          ref={swiperRef}
          cards={data.data}
          cardIndex={index}
          renderCard={card => <Card card={card} />}
          infinite
          backgroundColor={'transparent'}
          onSwiped={onSwiped}
          onTapCard={() => swiperRef.current.swipeLeft()}
          cardVerticalMargin={50}
          stackSize={stackSize}
          stackScale={10}
          stackSeparation={14}
          animateOverlayLabelsOpacity
          animateCardOpacity
          disableTopSwipe
          disableBottomSwipe
          overlayLabels={{
            left: {
              title: 'KHÔNG QUAN TÂM',
              style: {
                label: {
                  backgroundColor: colors.red,
                  borderColor: colors.red,
                  color: colors.white,
                  borderWidth: 1,
                  fontSize: 24
                },
                wrapper: {
                  flexDirection: 'column',
                  alignItems: 'flex-end',
                  justifyContent: 'flex-start',
                  marginTop: 20,
                  marginLeft: -20
                }
              }
            },
            right: {
              title: 'QUAN TÂM',
              style: {
                label: {
                  backgroundColor: colors.blue,
                  borderColor: colors.blue,
                  color: colors.white,
                  borderWidth: 1,
                  fontSize: 24
                },
                wrapper: {
                  flexDirection: 'column',
                  alignItems: 'flex-start',
                  justifyContent: 'flex-start',
                  marginTop: 20,
                  marginLeft: 20
                }
              }
            }
          }}
        />
      </View>
      )}
      
      <ScrollView style={styles.bottomContainer}>
      {isLoading ? <ActivityIndicator/> : (
        <Transitioning.View
          ref={transitionRef}
          transition={transition}
          style={styles.bottomContainerMeta}
        >
          <CardDetails index={index} />
        </Transitioning.View>
      )}
      </ScrollView>
      {isLoading ? <ActivityIndicator/> : (
      <View style={styles.bottomContainerButtons}>
          <MaterialCommunityIcons.Button
            name='close'
            size={70}
            backgroundColor='transparent'
            underlayColor='transparent'
            activeOpacity={0.3}
            color={colors.red}
            onPress={DontCare}
          />
          <MaterialCommunityIcons.Button
            name='circle-outline'
            size={70}
            backgroundColor='transparent'
            underlayColor='transparent'
            activeOpacity={0.3}
            color={colors.blue}
            onPress={Care}
          />
        </View>
        )}
    </SafeAreaView>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ddddff"
  },
  swiperContainer: {
    flex: 1,
  },
  bottomContainer: {
    flex: 1,
    padding: 10,
    // justifyContent: 'space-evenly'
  },
  bottomContainerMeta: { alignContent: 'flex-end', alignItems: 'center' },
  bottomContainerButtons: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    flex: 0.3,
  },
  cardImage: {
    width: 160,
    flex: 1,
    resizeMode: 'contain'
  },
  card: {
    flex: 0.45,
    borderRadius: 8,
    shadowRadius: 25,
    shadowColor: colors.black,
    shadowOpacity: 0.08,
    shadowOffset: { width: 0, height: 0 },
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white
  },
  text: {
    textAlign: 'center',
    fontSize: 30,
    backgroundColor: 'transparent'
  },
  done: {
    textAlign: 'center',
    fontSize: 30,
    color: colors.white,
    backgroundColor: 'transparent'
  },
  text: { fontFamily: 'Courier', fontSize: 22 },
  heading: { fontSize: 24, marginBottom: 10, color: colors.gray },
  price: { color: colors.blue, fontSize: 30, fontWeight: '500', textAlign: 'center' },
  titleInfo: {
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: '#fff',
    backgroundColor: '#b0e0e6',
    borderRadius: 8,
    shadowRadius: 25,
    margin: 5,
    fontSize: 20,
    color: "blue"
    
  },
  text1: {
    color: "#dd12ee",
  }
});

      
import React, { useState } from "react";
import { Keyboard, Button, TouchableOpacity, StyleSheet, Text, View, TextInput, TouchableWithoutFeedback, KeyboardAvoidingView, Alert } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { Loading } from '../components/loading';
import { Error } from '../components/error';
import { Heading } from '../components/heading';
import callApi from '../components/apiCaller';
import AsyncStorage from '@react-native-community/async-storage';
export default function SignInScreen({ navigation }) {
    {
        
        //set loading khi bam login
        const [loading, setLoading] = useState(false);

        const [error, setError] = useState('');
        //data nhập từ bàn phím
        const [data, setData] = useState({
            username: "",
            password: "",
            kq: "",
            check_textInputChange: false,
            secureTextEntry: true,
            isValidUser: true,
            isValidPassword: true,
        });
        
        //mở tắt nhìn mật khẩu
        const updateSecureTextEntry = () => {
            setData({
                ...data,
                secureTextEntry: !data.secureTextEntry
            });
        }
        //bắt sự kiện người dùng nhập từ bàn phím
        const handlePasswordChange = (val) => {
            setData({
                ...data,
                password: val,
                isValidPassword: false
            });

        }
        const handleEmailChange = (val) => {

            setData({
                ...data,
                username: val,
            });
        }
        //hàm đăng nhập

        const Login = () => {
            return callApi('api/candidate/login', 'POST', {
                email: data.username,
                password: data.password
            }, {
                'Content-Type': 'application/json',
                'X-Requested-With' : 'XMLHttpRequest'
            }).then(async (response) => {
                console.log(response.data.access_token);
                setLoading(false);
                
                if (response.status == 200) {
                    //await AsyncStorage.setItem('TokenUngVien',response.data.access_token);
                     //navigation.replace("CandidatesHome");
                     setError('Đăng nhập thành công!');
                     Alert.alert('Thông báo', 'Chức năng cho ứng viên chưa hoàn thiện. Bạn có muốn tiếp tục', [{
                        text: 'OK',
                        onPress: () => {
                            AsyncStorage.setItem('TokenUngVien',response.data.access_token);
                            navigation.replace("CandidatesHome");
                        }
                    }, {
                        text: 'Quay lại',
                    }],
                    
                    );

                }
                
                
            }).catch(error => {
                if(error.response.status == 401) {
                    setError('Tài khoản hoặc mật khẩu không chính xác!');
                    setLoading(false)
                }
                else if(error.response.status == 422) {
                    setError('Thông tin không hợp lệ hoặc thiếu vui lòng nhập lại!');
                    setLoading(false)
                }
                else {
                    Alert.alert('Thông báo', 'Lỗi kết nối đến máy chủ! Vui lòng thử lại sau' + error, [{
                        text: 'OK',
                        onPress: () => setLoading(false)
                    }]);
                }
                
                console.log(error);
            })
        }
        return (
            <KeyboardAvoidingView style={styles.containerView} behavior="padding">
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={styles.loginScreenContainer}>
                        <AntDesign name="arrowleft" color="#bbb" style={{ margin: 10 }} size={30} onPress={() => navigation.goBack()} />
                        <View style={styles.loginFormView}>
                            <Heading style={styles.title}>Đăng nhập ứng viên</Heading>
                            <Error error={error} />
                            <View style={styles.action}>
                                <FontAwesome
                                    name="user-o"
                                    color="#0066cc"
                                    size={20}
                                />
                                <TextInput
                                    placeholder="Email"
                                    placeholderColor="#c4c3cb"
                                    style={styles.loginFormTextInput}
                                    onChangeText={(val) => handleEmailChange(val)} />
                            </View>
                            <View style={styles.action}>
                                <Feather
                                    name="lock"
                                    color="#0066cc"
                                    size={20}
                                />
                                <TextInput placeholder="Mật Khẩu"
                                    placeholderColor="#c4c3cb"
                                    style={styles.loginFormTextInput} secureTextEntry={true}
                                    secureTextEntry={data.secureTextEntry ? true : false}
                                    autoCapitalize="none"
                                    onChangeText={(val) => handlePasswordChange(val)} />
                                <TouchableOpacity
                                    onPress={updateSecureTextEntry}
                                >
                                    {data.secureTextEntry ?
                                        <Feather
                                            name="eye-off"
                                            color="#0066cc"
                                            size={20}
                                        />
                                        :
                                        <Feather
                                            name="eye"
                                            color="grey"
                                            size={20}
                                        />
                                    }
                                </TouchableOpacity>
                            </View>
                            <TouchableOpacity>
                                <View style={styles.viewText}>
                                    <Text>Quên mật khẩu?</Text>
                                </View>
                            </TouchableOpacity>
                            
                            <View style={styles.loginButton}>
                                <Button

                                    onPress={async () => {
                                        try {
                                            setLoading(true);
                                            await Login();
                                        } catch (e) {
                                            console.log(e);   
                                        }
                                    }}
                                    title="Đăng nhập"
                                />
                            </View>
                            <View style={styles.viewResgi}>
                                <Text style={styles.colorText}>Bạn chưa có tài khoản?</Text>
                                <TouchableOpacity onPress={() => navigation.navigate("SignUpScreen")}>
                                    <Text style={styles.colorText}> ĐĂNG KÝ</Text>
                                </TouchableOpacity>
                            </View>
                            <Loading loading={loading} />
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        );
    }
}
const styles = StyleSheet.create({
    containerView: {
        flex: 1,
    },
    loginScreenContainer: {
        flex: 1,
    },

    loginFormView: {
        flex: 1
    },
    loginFormTextInput: {
        height: 43,
        fontSize: 16,
        paddingLeft: 10,
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        color: '#05375a',


    },
    loginButton: {
        backgroundColor: '#3897f1',
        borderRadius: 10,
        margin: 20,
    },
    action: {
        flexDirection: 'row',
        padding: 5,
        borderRadius: 5,
        borderBottomWidth: 1,
        borderColor: '#0066cc',
        margin: 5,
        marginLeft: 15,
        marginRight: 15,
        marginBottom: 10,
    },
    viewText: {
        alignItems: 'flex-end',
        marginRight: 30,
        marginTop: 10,
    },
    viewResgi: {
        flexDirection: "row",
        paddingHorizontal: 100,

    },
    colorText: {
        color: '#0066cc',
        fontSize: 16,
    }
});
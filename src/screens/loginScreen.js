import React from 'react';
import { View, Text, StyleSheet, Button, ImageBackground } from 'react-native';
import { useState } from 'react';
import AsyncStorage from '@react-native-community/async-storage';


function LoginScreen({ navigation }) {
    
        React.useEffect(() => {
            async function checkToken() {
              try {
                const tokenRecruitment = await AsyncStorage.getItem('Token');
                const tokenUngVien = await AsyncStorage.getItem('TokenUngVien');
                if(tokenRecruitment!= null){
                  navigation.replace('RecruiterHome');
                }
                if(tokenUngVien != null)
                {
                  navigation.replace('CandidatesHome');
                }
              }
              catch (e) {
                console.log(e);
              }
            }
        
            checkToken();
        
          });
    return (
        
        <View style={styles.container}>
            <ImageBackground  source={require('../images/hinhnen.png')} style={styles.image}>
                <Text style={styles.text1}>Welcome to</Text>
                <Text style={styles.text}>Hệ thống gợi ý việc làm</Text>
                <Text style={styles.text2}>Bạn là ứng viên hay nhà tuyển dụng?</Text>
                <View style={styles.button}>
                    <Button title="Ứng viên" color="#23ABDC"  onPress={() => navigation.navigate('SignInScreen')}/>
                </View>
                <View style={styles.button}>
                    <Button title="Nhà tuyển dụng" color="#6633ff" onPress={() => navigation.navigate('SignInScreenRecruiter')} />
                </View>
            </ImageBackground>
        </View>

    );
}
export default LoginScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
    alignItems:'center',
  },
  text: {
      fontWeight: 'bold',
      fontSize: 35,
      color: 'yellow',
      marginBottom:60,
  },
  text1: {
    fontSize: 35,
    color: '#fff',
  },
  text2: {
    margin: 20,
    fontSize: 18,
    color: '#ddd',
  },
  button: {
    width: 250,
    margin: 10,
  }
});

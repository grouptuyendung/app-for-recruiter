import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Job from '../components/job';
import Profile from '../components/candidatesProfile';
import FindJob from '../components/findJob';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const Tab = createBottomTabNavigator();
function CandidatesHomeScreen () {
    return(
      <Tab.Navigator 
        initialRouteName="findCandidates"
        tabBarOptions={{
          activeTintColor:'#1e88e5',
          labelStyle:{fontSize:12}    
      }}>
        <Tab.Screen name="findCandidates" component={FindJob}
          options={{
          tabBarVisible:true,
          tabBarLabel: 'Tìm việc',
          tabBarIcon: ({color})=> <FontAwesome name="search" size={32} color={color}/>
        }} />
        <Tab.Screen name="recruiter" component={Job} options={{
          tabBarLabel: 'Việc làm',
          tabBarIcon: ({color})=> <FontAwesome5 name="user-tie" size={32} color={color}/>
        }} />
        <Tab.Screen name="profile" component={Profile} options={{
          tabBarLabel: 'Hồ sơ',
          tabBarIcon: ({color})=> <FontAwesome5 name="user-edit" size={32} color={color}/>
        }} />
      </Tab.Navigator>
    );
}
export default CandidatesHomeScreen;

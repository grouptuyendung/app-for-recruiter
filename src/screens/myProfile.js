import React from 'react';
import { StyleSheet, Text, View, StatusBar, Button, } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { ScrollView } from 'react-native-gesture-handler';
import AntDesign from 'react-native-vector-icons/AntDesign';

export default function Profile ({navigation}) {
    return(
      <View style={styles.screenContainer}>
      <StatusBar barStyle="light-content" />
      {/*  */}
      <View style={styles.headerContainer}>
        {/*  */}
        <View style={styles.cartContainer}>
          <View style={styles.cartIcon} />
          <AntDesign name="arrowleft" color="white" size={25} onPress={() => navigation.goBack()} />
        </View>
        {/*  */}
        <Text style={styles.headerText}>Hồ sơ</Text>
        {/*  */}
        <View style={styles.cartContainer}>
          
        </View>
      </View>
      {/*  */}
      <ScrollView>
      <View style={styles.bodyContainer}>
        <View style={styles.userContainer}>
        <View style={styles.avatarContainer}>
            <MaterialIcons name="person" size={80} color="#fff" />
          </View>
          <View style={styles.textinfo}>
            <Text style={{fontSize: 18,fontWeight: 'bold',marginBottom: 5,}}>Nguyễn Văn A</Text>
            <Text style={{fontSize: 14,marginBottom: 5,}}>Email: abc@gmail.com</Text>
            <Text style={{fontSize: 14,marginBottom: 5,}}>Số điện thoại: 123456789</Text>
            <Text style={{fontSize: 14,marginBottom: 5,}}>Ngày sinh: 1/1/2000</Text>
            <Text style={{fontSize: 14,marginBottom: 5,}}>Giới tính: Nam</Text>
            <Text style={{fontSize: 14,}}>Địa chỉ: TP Hồ Chí Minh</Text>
          </View>
          <View >
              <FontAwesome5 name='edit' size ={30}/>
          </View>
        </View>
        <View style={styles.hoso}>
            <View >
                <Text style={{fontSize: 20, color:'#1e88e5'}}>Hồ sơ đính kèm</Text>
            </View>
            <View style={{flexDirection: 'row',justifyContent:'flex-end'}}>
                <View style={{marginRight: 10,}}>
                    <Button title='Xem CV'  />
                </View>
                <View>
                    <Button title='Thêm CV' />
                </View>
            </View>
        </View>
        <View style={styles.congviecmongmuon}>
            <View style={{borderBottomWidth:1,margin:5,}} >
                <Text style={{fontSize: 20, color:'#1e88e5'}}>Công việc mong muốn</Text>
            </View>
            <View style={{borderBottomWidth:1,}}>
                <Text style={{color:'#999',margin: 5,fontSize: 15}}>Công việc mong muốn</Text>
                <Text style={{margin: 5,fontSize: 18,fontWeight:'bold'}}>React native fresher</Text>
            </View>
            <View style={{borderBottomWidth:1,}}>
                <Text style={{color:'#999',margin: 5,fontSize: 15}}>Hình thức làm việc</Text>
                <Text style={{margin: 5,fontSize: 18,fontWeight:'bold'}}>Toàn thời gian</Text>
            </View>
            <View style={{borderBottomWidth:1,}}>
                <Text style={{color:'#999',margin: 5,fontSize: 15}}>Mức lương mong muốn</Text>
                <Text style={{margin: 5,fontSize: 18,fontWeight:'bold'}}>Thỏa thuận</Text>
        
            </View>
            <View style={{margin: 10,flexDirection: 'row',justifyContent:'flex-end'}}>
                    <Button title='Cập nhật'  />
                </View>
        </View>
        <View style={styles.hoso}>
            <View style={{borderBottomWidth:1,}}>
                <Text style={{fontSize: 20, color:'#1e88e5'}}>Mục tiêu nghề nghiệp</Text>
            </View>
            <View >
                <Text style={{color:'#999',margin: 5,fontSize: 15}}>Việc nhẹ lương cao</Text>
            </View>
            <View style={{margin: 10,flexDirection: 'row',justifyContent:'flex-end'}}>
                    <Button title='Cập nhật'  />
                </View>
            <View>
               
            </View>
        </View>
        <View style={styles.hoso}>
            <View style={{borderBottomWidth:1,}}>
                <Text style={{fontSize: 20, color:'#1e88e5'}}>Kĩ năng bản thân</Text>
            </View>
            <View >
                <Text style={{color:'#999',margin: 5,fontSize: 15}}>Giao tiếp tiếng anh, desgin</Text>
            </View>
            <View style={{margin: 10,flexDirection: 'row',justifyContent:'flex-end'}}>
                    <Button title='Cập nhật'  />
                </View>
            <View>
               
            </View>
        </View>
        <View style={styles.hoso}>
            <View style={{borderBottomWidth:1,}}>
                <Text style={{fontSize: 20, color:'#1e88e5'}}>Bằng cấp chứng chỉ</Text>
            </View>
            <View >
                <Text style={{color:'#999',margin: 5,fontSize: 15}}>Toeic 900</Text>
            </View>
            <View style={{margin: 10,flexDirection: 'row',justifyContent:'flex-end'}}>
                    <Button title='Cập nhật'  />
                </View>
            <View>
               
            </View>
        </View>
      </View>
      </ScrollView>
    </View>
      );
}
const HEADER_ICON_SIZE = 24;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
  },
  headerContainer: {
    flexDirection: 'row',
    paddingTop: 10,
    backgroundColor: '#1e88e5',
    justifyContent: 'space-between',
    paddingBottom: 12,
  },
  cartContainer: {
    paddingHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cartIcon: {
    width: HEADER_ICON_SIZE,
  },
  headerText: {
    color: '#fff',
    fontSize: 20,
    fontWeight: '500',
  },
  //
  bodyContainer: {
    flex: 1,
    backgroundColor: '#ddd',
  },
  //
  userContainer: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingHorizontal: 30,
    paddingVertical: 22,
    alignItems: 'center',
    margin:10,
    borderRadius: 10,
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 2},
      shadowOpacity: 0.25,
      shadowRadius: 4,
  },
  avatarContainer: {
    width: 100,
    height: 100,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black',
  },
  textContainer: {
    flex: 1,
    marginLeft: 20,
  },
  nameText: {
    color: '#1e88e5',
    fontSize: 18,
    fontWeight: '500',
  },
  gmailText: {
    color: '#828282',
  },
  divider: {
    height: 10,
  },
  itemContainer: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 10,
    alignItems: 'center',
  },
  hoso: {
    backgroundColor: '#fff',
    paddingHorizontal: 30,
    paddingVertical: 22,
    margin:10,
    borderRadius: 10,
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 2},
      shadowOpacity: 0.25,
      shadowRadius: 4,

  },
  textinfo:{
      flex: 1,
      margin: 10,
  },
  congviecmongmuon: {
    backgroundColor: '#fff',
    paddingHorizontal: 30,
    paddingVertical: 22,
    margin: 10,
    borderRadius: 10,
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 2},
      shadowOpacity: 0.25,
      shadowRadius: 4,
  },
});
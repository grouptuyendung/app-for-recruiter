import React, { useState } from 'react';
import { StyleSheet, Text, View, StatusBar, Button, Alert, TextInput, } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { ScrollView } from 'react-native-gesture-handler';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { Loading } from '../components/loading';
import { Error } from '../components/error';
import callApi from '../components/apiCaller';
import AsyncStorage from '@react-native-community/async-storage';
export default function UpdateProfile ({route, navigation}) {
  const {id, name, email, address, phoneNumber} = route.params;
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const [data, setData] = useState({
    id: id,
    name: name,
    email: email,
    address: address,
    phoneNumber: phoneNumber,
});
const handleEmailChange = (val) => {

  setData({
      ...data,
      email: val,
  });
  console.log(data.email);
}
const handlePhoneNumberChange = (val) => {

  setData({
      ...data,
      phoneNumber: val,
  });
  console.log(data.phoneNumber);
}
const handleAddressChange = (val) => {

  setData({
      ...data,
      address: val,
  });
  console.log(data.address);
}
const handleNameChange = (val) => {

  setData({
      ...data,
      name: val,
  });
  console.log(data.name);
}
const userToken = AsyncStorage.getItem('Token');
const Update = () => {
  return callApi('api/user/updateinfo', 'POST', {
      id: data.id,
      name: data.name,
      address: data.address,
      phoneNumber: data.phoneNumber,
  }, {
      'Authorization': 'Bearer ' + userToken._W,
  }).then(async (response) => {
      console.log(response.data);
      setLoading(false);
      
      if (response.status == 200) {
          
           setError('Cập nhập thành công!');         
           setLoading(false)
           //navigation.goBack();
           console.log(data);
      }
      
      
  }).catch(error => {
      if(error.response.status == 401) {
          setError('Cập nhật thất bại!');
          setLoading(false);
          console.log(data);
          console.log(userToken);
      }
      else if(error.response.status == 422) {
          setError('Cập nhật thất bại!');
          setLoading(false);
          console.log(data);
      }
      else {
          Alert.alert('Thông báo', 'Lỗi kết nối đến máy chủ! Vui lòng thử lại sau' + error, [{
              text: 'OK',
              onPress: () => {setLoading(false); console.log(data);}
          }]);
      }
      
      console.log(error);
  })
}
    return(
      <View style={styles.screenContainer}>
      <StatusBar barStyle="light-content" />
      {/*  */}
      <View style={styles.headerContainer}>
        {/*  */}
        <View style={styles.cartContainer}>
          <View style={styles.cartIcon} />
          <AntDesign name="arrowleft" color="white" size={25} onPress={() => navigation.goBack()} />
        </View>
        {/*  */}
        <Text style={styles.headerText}>Hồ sơ của tôi</Text>
        {/*  */}
        <View style={styles.cartContainer}>
          
        </View>
      </View>
      {/*  */}
      <ScrollView>
          <View style={styles.bodyContainer}>
              <View style={styles.userContainer}>
                <View style={styles.avatarContainer}>
                    <MaterialIcons name="person" size={80} color='#fff'/>
                </View>                
              </View>
              <View style={styles.hoso}>
              <Error error={error} />
                <View style={{borderBottomWidth:1,margin:5,}} >
                    <Text style={{fontSize: 20, color:'#1e88e5'}}>Thông tin nhà tuyển dụng</Text>
                </View>
                <View>
                    <Text style={{color:'#999',margin: 5,fontSize: 15}}>Họ và tên:</Text>
                    <TextInput style={styles.textInfo} onChangeText={(val) => handleNameChange(val)} defaultValue={name} placeholder="Nhập họ và tên"/>
                </View>
                <View>
                    <Text style={{color:'#999',margin: 5,fontSize: 15}}>Email:</Text>
                    <Text style={styles.textInfo1}>{email}</Text>
                </View>
                <View>
                    <Text style={{color:'#999',margin: 5,fontSize: 15}}>Số điện thoại:</Text>
                    <TextInput style={styles.textInfo} onChangeText={(val) => handlePhoneNumberChange(val)} defaultValue={phoneNumber} placeholder="Nhập số điện thoại công ty"/>
                </View>
                {/* <View>
                    <Text style={{color:'#999',margin: 5,fontSize: 15}}>Tên công ty:</Text>
                    <TextInput style={styles.textInfo} defaultValue="Công Ty MTH ABC" placeholder="Nhập tên công ty"/>
                </View> */}
                {/* <View>
                    <Text style={{color:'#999',margin: 5,fontSize: 15}}>Giới thiệu về công ty:</Text>
                    <TextInput style={styles.textInfo} multiline={true} defaultValue="Cần tuyển những thực tập viên ...." placeholder="Giới thiệu về công ty"/>
                </View> */}
                <View>
                    <Text style={{color:'#999',margin: 5,fontSize: 15}}>Địa chỉ:</Text>
                    <TextInput style={styles.textInfo} onChangeText={(val) => handleAddressChange(val)} defaultValue={address} placeholder="Nhập địa chỉ công ty" />
                </View>
                <View style={{margin: 10,flexDirection: 'row',justifyContent:'flex-end'}}>
                    <Button title='Cập nhật' onPress={async () => {
                                        try {
                                            setLoading(true);
                                            await Update();
                                        } catch (e) {
                                            console.log(e);   
                                        }
                                    }}/>
                </View>
              </View>
              <Loading loading={loading} />
          </View>
      </ScrollView>
    </View>
      );
}
const HEADER_ICON_SIZE = 24;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
  },
  headerContainer: {
    flexDirection: 'row',
    paddingTop: 10,
    backgroundColor: '#1e88e5',
    justifyContent: 'space-between',
    paddingBottom: 12,
  },
  cartContainer: {
    paddingHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cartIcon: {
    width: HEADER_ICON_SIZE,
  },
  headerText: {
    color: '#fff',
    fontSize: 20,
    fontWeight: '500',
  },
  //
  bodyContainer: {
    flex: 1,
    backgroundColor: '#ddd',
  },
  //
  userContainer: {
    flex:1,
    backgroundColor: '#fff',
    paddingHorizontal: 30,
    paddingVertical: 22,
    alignItems: 'center',
    margin:10,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.25,
    shadowRadius: 4,
  },
  avatarContainer: {
    width: 100,
    height: 100,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black',
  },
  textContainer: {
    flex: 1,
    marginLeft: 20,
  },
  nameText: {
    color: '#1e88e5',
    fontSize: 18,
    fontWeight: '500',
  },
  gmailText: {
    color: '#828282',
  },
  divider: {
    height: 10,
  },
  itemContainer: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 10,
    alignItems: 'center',
  },
  hoso: {
    backgroundColor: '#fff',
    paddingHorizontal: 30,
    paddingVertical: 22,
    margin:10,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.25,
    shadowRadius: 4,

  },
  textInfo: {
    margin: 5,
    fontSize: 18,
    fontWeight:'bold',
    borderWidth: 1,
    padding: 10,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.25,
    shadowRadius: 2,
    borderColor: '#999'
  },
  textInfo1: {
    margin: 5,
    fontSize: 18,
    color: '#aaa',
    fontWeight:'bold',
    borderWidth: 1,
    padding: 10,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.25,
    shadowRadius: 2,
    borderColor: '#999'
  }
});
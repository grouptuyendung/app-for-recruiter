import React from "react";
import { Keyboard, Button, TouchableOpacity, StyleSheet, Text, View, TextInput, TouchableWithoutFeedback, Alert, KeyboardAvoidingView } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { ScrollView } from "react-native-gesture-handler";
import { Loading } from '../components/loading';
import { Error } from '../components/error';
import { Heading } from '../components/heading';
import callApi from '../components/apiCaller';
export default function SignUpScreenRecruiter({ navigation }) {
    {
        const [loading, setLoading] = React.useState(false);
        const [error, setError] = React.useState('');
        const [data, setData] = React.useState({
            username: '',
            password: '',
            password2: '',
            fullname: '',
            phoneNumber: '',
            address: '',
            
            check_textInputChange: false,
            secureTextEntry: true,
            isValidUser: true,
            isValidPassword: true,
        });
        const updateSecureTextEntry = () => {
            setData({
                ...data,
                secureTextEntry: !data.secureTextEntry
            });
        }
        const handlePasswordChange = (val) => {
            setData({
                ...data,
                password: val,
                isValidPassword: false
            });

        }
        const handleEmailChange = (val) => {

            setData({
                ...data,
                username: val,
            });
        }
        const handlePassword2Change = (val) => {

            setData({
                ...data,
                password2: val,
            });
        }
        const handleFullnameChange = (val) => {

            setData({
                ...data,
                fullname: val,
            });
             
        }
        const handlePhoneChange = (val) => {

            setData({
                ...data,
                phoneNumber: val,
            });
        }
        const handleAddressChange = (val) => {

            setData({
                ...data,
                address: val,
            });
        }
        
        const SignUp = () => {
            return callApi('api/user/register', 'POST', {
                email: data.username,
                password: data.password,
                name: data.fullname,
                password_confirmation: data.password2,
                address: data.address,
                phoneNumber: data.phoneNumber,
                role_id: 2,
            }, {
                'Content-Type': 'application/json',
                'X-Requested-With' : 'XMLHttpRequest'
            }).then(async (response) => {
                console.log(response.data);
                setLoading(false);
                
                if (response.status == 201) {
                    // await AsyncStorage.setItem('Token',response.data.access_token);
                    
                     
                     Alert.alert('Thông báo', "Đăng ký thành công vui lòng đăng nhập để sử dụng ứng dụng", [{
                        text: 'OK',
                        onPress: () => navigation.replace("SignInScreenRecruiter")
                    }]);
                     
                }
                
                
            }).catch(error => {
                console.log(error);
                
                    Alert.alert('Thông báo', 'Thông tin đăng ký không chính xác! Vui lòng thử lại sau' + error, [{
                        text: 'OK',
                        onPress: () => setLoading(false)
                    }]);
                
                
            })
        }
        return (
            <KeyboardAvoidingView style={styles.containerView} behavior="padding">
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <ScrollView style={styles.loginScreenContainer}>
                    <AntDesign name="arrowleft" color="#bbb" style={{margin: 10}} size={30} onPress={() => navigation.goBack()} />
                        <View style={styles.loginFormView}>
                        <Heading>Đăng ký tài khoản nhà tuyển dụng</Heading>
                            <Error error={error} />
                            <Text style={styles.titleInput}>Email (Dùng để đăng nhập)</Text>
                            <View style={styles.action}>
                                <AntDesign
                                    name="mail"
                                    color="#0066cc"
                                    size={20}
                                />
                                <TextInput 
                                    placeholder="Email" 
                                    placeholderColor="#c4c3cb"
                                    style={styles.loginFormTextInput}    
                                    onChangeText={(val) => handleEmailChange(val)}
                                             />
                            </View>
                            <Text style={styles.titleInput}>Mật khẩu</Text>
                            <View style={styles.action}>
                                <Feather
                                    name="lock"
                                    color="#0066cc"
                                    size={20}
                                />
                                <TextInput
                                    
                                    placeholder="Mật Khẩu"
                                    placeholderColor="#c4c3cb"
                                    style={styles.loginFormTextInput}
                                    secureTextEntry={true}
                                    secureTextEntry={data.secureTextEntry ? true : false}
                                    autoCapitalize="none"
                                    onChangeText={(val) => handlePasswordChange(val)} />
                                <TouchableOpacity
                                    onPress={updateSecureTextEntry}
                                >
                                    {data.secureTextEntry ?
                                        <Feather
                                            name="eye-off"
                                            color="#0066cc"
                                            size={20}
                                        />
                                        :
                                        <Feather
                                            name="eye"
                                            color="grey"
                                            size={20}
                                        />
                                    }
                                </TouchableOpacity>
                            </View>
                            <Text style={styles.titleInput}>Nhập lại mật khẩu</Text>
                            <View style={styles.action}>
                                <Feather
                                    name="lock"
                                    color="#0066cc"
                                    size={20}
                                />
                                <TextInput
                                
                                    placeholder="Nhập lại mật Khẩu"
                                    placeholderColor="#c4c3cb"
                                    style={styles.loginFormTextInput}
                                    secureTextEntry={true}
                                    secureTextEntry={data.secureTextEntry ? true : false}
                                    autoCapitalize="none"
                                    onChangeText={(val) => handlePassword2Change(val)} />
                                <TouchableOpacity
                                    onPress={updateSecureTextEntry}
                                >
                                    {data.secureTextEntry ?
                                        <Feather
                                            name="eye-off"
                                            color="#0066cc"
                                            size={20}
                                        />
                                        :
                                        <Feather
                                            name="eye"
                                            color="grey"
                                            size={20}
                                        />
                                    }
                                </TouchableOpacity>
                            </View>
                            <Text style={styles.titleInput}>Họ tên</Text>
                            <View style={styles.action}>
                                <FontAwesome
                                    name="user-o"
                                    color="#0066cc"
                                    size={20}
                                />
                                <TextInput placeholder="Nhập họ tên nhà tuyển dụng" 
                                             onChangeText={(val) => handleFullnameChange(val)}                                        
                                            placeholderColor="#c4c3cb" style={styles.loginFormTextInput} />
                            </View>
                            {/* <Text style={styles.titleInput}>Tên công ty</Text>
                            <View style={styles.action}>
                                <FontAwesome
                                    name="building"
                                    color="#0066cc"
                                    size={20}
                                />
                                <TextInput placeholder="Nhập tên công ty" placeholderColor="#c4c3cb" style={styles.loginFormTextInput} />
                            </View> */}
                            <Text style={styles.titleInput}>Số điện thoại</Text>
                            <View style={styles.action}>
                                <FontAwesome
                                    name="phone"
                                    color="#0066cc"
                                    size={20}
                                />
                                <TextInput  onChangeText={(val) => handlePhoneChange(val)} keyboardType="number-pad" placeholder="Số điện thoại" placeholderColor="#c4c3cb" style={styles.loginFormTextInput} />
                            </View>
                            <Text style={styles.titleInput}>Địa chỉ</Text>
                            <View style={styles.action}>
                                <FontAwesome
                                    name="address-book"
                                    color="#0066cc"
                                    size={20}
                                />
                                <TextInput  onChangeText={(val) => handleAddressChange(val)} placeholder="Địa chỉ công ty" placeholderColor="#c4c3cb" style={styles.loginFormTextInput} />
                            </View>
                            {/* <Text style={styles.titleInput}>Giới thiệu công ty</Text>
                            <View style={styles.action1}>
                                <FontAwesome
                                    name="pencil-square-o"
                                    color="#0066cc"
                                    size={20}
                                />
                                <TextInput multiline={true} placeholder="Giới thiệu công ty" placeholderColor="#c4c3cb" style={styles.loginFormTextInput} />
                            </View> */}
                            <View style={styles.loginButton}>
                                <Button
                                    onPress={async () => {
                                        try {
                                            setLoading(true);
                                            await SignUp();
                                        } catch (e) {
                                            console.log(e);
                                        }
                                    }}
                                    title="Đăng ký"
                                />
                            </View>
                            <Loading loading={loading} />
                        </View>
                    </ScrollView>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        );
    }
}
const styles = StyleSheet.create({
    containerView: {
        flex: 1,
    },
    loginScreenContainer: {
        flex: 1,
    },
    logoText: {
        fontSize: 25,
        fontWeight: "800",
        marginTop: 30,
        marginBottom: 30,
        textAlign: 'center',
    },
    loginFormView: {
        flex: 1
    },
    loginFormTextInput: {
        height: 43,
        fontSize: 16,
        paddingLeft: 10,
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        color: '#05375a',


    },
    loginButton: {
        backgroundColor: '#3897f1',
        borderRadius: 10,
        margin: 20,
    },
    action: {
        flexDirection: 'row',
        padding: 5,
        borderRadius: 5,
        borderBottomWidth: 1,
        borderColor: '#0066cc',
        margin: 5,
        marginLeft: 15,
        marginRight: 15,
        marginBottom: 10,
    },
    viewText: {
        alignItems: 'flex-end',
        marginRight: 30,
        marginTop: 10,
    },
    viewResgi: {
        flexDirection: "row",
        paddingHorizontal: 100,
        
    },
    colorText: {
        color: '#0066cc',
        fontSize: 16,
    },
    titleInput: {
        color: "black",
        fontSize: 16,
        margin: 5,
        paddingLeft: 15,
    },
    action1: {
        flexDirection: 'row',
        padding: 5,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#0066cc',
        margin: 5,
        marginLeft: 15,
        marginRight: 15,
        marginBottom: 10,
        height: 100,
    }
});
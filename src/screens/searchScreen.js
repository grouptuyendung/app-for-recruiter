import React, {useEffect, useState} from 'react';
import {View, Button, TextInput, RefreshControl, ActivityIndicator, FlatList, Text, Image, TouchableOpacity, StyleSheet, Alert} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AsyncStorage from '@react-native-community/async-storage';
import callApi from '../components/apiCaller';
import { Loading } from '../components/loading';
import { useNavigation } from '@react-navigation/native';
function SearchDetail() {
    const [data, setData] = useState('');
    const navigation = useNavigation();
    const handleSearchChange = (val) => {

        setData(val);
        console.log(val);
    }
    const [listCard, setListCard] = useState();
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState('Không tìm thấy ứng viên');
    const [loading, setLoading] = useState(true);
    
    const getCV = () => {
       callApi('api/searchcv', 'POST',  {
        search: data,
    }, {
        'Content-Type': 'application/json',
        'X-Requested-With' : 'XMLHttpRequest'
    }).then(async (responseJson) => {
      //console.log(responseJson.data);
        
        await setListCard(responseJson.data.data);
       
      
      setLoading(false);
      setIsLoading(false);
      //setFetching(false);
    }).catch(err => {
      console.log(err);
      setError(JSON.stringify(err.message));
      console.log(data);
    });
    
    }
    useEffect(() => {
      
      
     
     console.log('abc');
     
    }, []);
    const onRefresh = () => {
        setLoading(true);
        setIsLoading(true);
      //setFetching(true);
      if(data === '') {
        Alert.alert('Thông báo','Vui lòng nhập từ khóa vào ô tìm kiếm ');
        setIsLoading(false);
    }else {
        getCV();
    }
    
    };
    return (
        <View style={styles.container}>
      <View style={styles.headerContainer}>
            <View style={styles.inputContainer2}>
                <TextInput  placeholder="Seach job here...."
                                    placeholderColor="#c4c3cb"
                                    style={styles.search}
                                    onChangeText={(val) => handleSearchChange(val)} />
            </View>

            <View style={styles.buttonStyle}>
                <Button title="Tìm kiếm" color="#FF8000"  onPress={() => onRefresh()}/>
                <MaterialIcons name="close" size={25} color="white" style={{ margin: 30 }} onPress={() => navigation.goBack()} />
            </View>
            
        </View>
        {loading ? <Loading loading={isLoading} /> : (
        <View style={styles.listCard}>
          {/* Dùng Flatlist show danh sách product */}
          <FlatList
            data={listCard}
            // refreshControl={
            //   <RefreshControl
            //     refreshing={fetching}
            //     onRefresh={() => onRefresh()}
            //   />
            // }
            ListEmptyComponent={
            <View>
              <Text style={styles.title}>{error}</Text>
     
            </View>
            }
            keyExtractor={(item, index) => String(index)}
            renderItem={({ item }) => (
              <View style={styles.cardView}>
                <TouchableOpacity style={styles.cardTouch} onPress={() => navigation.navigate('Detail', {cardDetail: item})} >
                  <View style={styles.cardGroup}>
                    <View style={styles.cardImage} >
                      <Image source={require('../images/avatar.png')} style={styles.avatarImage} />
                    </View>
                    <View style={styles.cardText}>
                      <Text style={styles.cardName}>{item.full_name}</Text>
                      <Text style={styles.cardPrice}>{item.fields}</Text>
                      <Text style={styles.cardPrice}>{item.level_desired}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          />
        </View>
        )}
      </View>
    );
  }


export default SearchDetail;
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
    },
    headerButton: {
      margin: 10,
      backgroundColor: '#333',
    },
    cardGroup: {
      borderWidth: 1,
      flexDirection: "row",
      margin: 15,
      padding: 5,
      backgroundColor: '#fff',
      borderRadius: 10,
      shadowColor: '#fff',
      shadowOffset: {width: 0, height: 2},
      shadowOpacity: 0.2,
      shadowRadius: 4,
    },
    cardText: {
      flex: 3,
    },
    avatarImage: {
      width: 100,
      height: 120,
      margin: 5,
      
    },
    cardImage: {
      flex: 2,
    },
    cardImageHorizontal: {
      alignItems:"center"
    }, 
    cardName: {
      fontWeight: "bold",
    },
    cardGroupHorizontal: {
      borderWidth: 1,
      margin: 5,
      padding: 5,
      width: 200,
      backgroundColor: '#fff',
      borderRadius: 10,
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 2},
      shadowOpacity: 0.25,
      shadowRadius: 4,
    },
    cardTextHorizontal: {
      alignItems: "center"
    },
    cardView: {
      backgroundColor: '#ddd'
    },
    title: {
      fontSize: 20,
      textAlign: 'center',
      color: '#444',
    },
    loginButton: {
      backgroundColor: '#3897f1',
      borderRadius: 15,
      margin: 40,
  },
  inputContainer2: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    flex: 1,
    margin: 10,
    marginRight: 100,
    padding: 20,
    alignItems: 'center',
    paddingHorizontal: 8,
    borderRadius: 2,
},
buttonStyle: {

    flexDirection: 'row',
    flex: 1,
    margin: 5,
    marginLeft: 0,
    marginRight: 90,
    padding: 20,
    alignItems: 'center',
    paddingHorizontal: 8,
    borderRadius: 2,
},
Icon: {
    margin: 30
},
headerContainer: {
    flexDirection: "column",
    paddingTop: 4,
    paddingBottom: 4,
    backgroundColor: '#1e88e5',
},
search: {
    height: 43,
    fontSize: 18,
    padding: 10,
    flex: 1,
    
    // marginTop: Platform.OS === 'ios' ? 0 : -12,
    color: '#05375a',


},
listCard: {
    flex: 1,
}
  });
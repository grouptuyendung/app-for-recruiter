import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Profile from '../components/profile';
import Recruiter from '../components/recruiter';
import FindCandidates from '../components/findCandidates';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const Tab = createBottomTabNavigator();
function HomeScreen () {
    return(
      <Tab.Navigator 
        initialRouteName="findCandidates"
        tabBarOptions={{
          activeTintColor:'#1e88e5',
          labelStyle:{fontSize:12}    
      }}>
        <Tab.Screen name="findCandidates" component={FindCandidates}
          options={{
          tabBarVisible:true,
          tabBarLabel: 'Ứng viên',
          tabBarIcon: ({color})=> <FontAwesome name="search" size={32} color={color}/>
        }} />
        <Tab.Screen name="recruiter" component={Recruiter} options={{
          tabBarLabel: 'Nhà tuyển dụng',
          tabBarIcon: ({color})=> <FontAwesome5 name="user-tie" size={32} color={color}/>
        }} />
        <Tab.Screen name="profile" component={Profile} options={{
          tabBarLabel: 'Hồ sơ',
          tabBarIcon: ({color})=> <FontAwesome5 name="user-edit" size={32} color={color}/>
        }} />
      </Tab.Navigator>
    );
}
export default HomeScreen;

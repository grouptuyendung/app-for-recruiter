import React from 'react';
import { StyleSheet, Text, View, StatusBar, Button, } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { ScrollView } from 'react-native-gesture-handler';
import AntDesign from 'react-native-vector-icons/AntDesign';
export default function Profile ({route, navigation}) {
  const { id, name, email, address, phoneNumber } = route.params;
    return(
      <View style={styles.screenContainer}>
      <StatusBar barStyle="light-content" />
      {/*  */}
      <View style={styles.headerContainer}>
        {/*  */}
        <View style={styles.cartContainer}>
          <View style={styles.cartIcon} />
          <AntDesign name="arrowleft" color="white" size={25} onPress={() => navigation.goBack()} />
        </View>
        {/*  */}
        <Text style={styles.headerText}>Hồ sơ của tôi</Text>
        {/*  */}
        <View style={styles.cartContainer}>
          
        </View>
      </View>
      {/*  */}
      <ScrollView>
          <View style={styles.bodyContainer}>
              <View style={styles.userContainer}>
                <View style={styles.avatarContainer}>
                    <MaterialIcons name="person" size={80} color='#fff'/>
                </View>                
              </View>
              <View style={styles.hoso}>
                <View style={{borderBottomWidth:1,margin:5,}} >
                    <Text style={{fontSize: 20, color:'#1e88e5'}}>Thông tin nhà tuyển dụng</Text>
                </View>
                <View style={{borderBottomWidth:1,}}>
                    <Text style={{color:'#999',margin: 5,fontSize: 15}}>Họ và tên:</Text>
                    <Text style={{margin: 5,fontSize: 18,fontWeight:'bold'}}>{name}</Text>
                </View>
                <View style={{borderBottomWidth:1,}}>
                    <Text style={{color:'#999',margin: 5,fontSize: 15}}>Email:</Text>
                    <Text style={{margin: 5,fontSize: 18,fontWeight:'bold'}}>{email}</Text>
                </View>
                <View style={{borderBottomWidth:1,}}>
                    <Text style={{color:'#999',margin: 5,fontSize: 15}}>Số điện thoại:</Text>
                    <Text style={{margin: 5,fontSize: 18,fontWeight:'bold'}}>{phoneNumber}</Text>
                </View>
                {/* <View style={{borderBottomWidth:1,}}>
                    <Text style={{color:'#999',margin: 5,fontSize: 15}}>Tên công ty:</Text>
                    <Text style={{margin: 5,fontSize: 18,fontWeight:'bold'}}>Công Ty MTH ABC </Text>
                </View>
                <View style={{borderBottomWidth:1,}}>
                    <Text style={{color:'#999',margin: 5,fontSize: 15}}>Giới thiệu về công ty:</Text>
                    <Text style={{margin: 5,fontSize: 18,fontWeight:'bold'}}>Cần tuyển những thực tập viên ....</Text>
                </View> */}
                <View style={{borderBottomWidth:1,}}>
                    <Text style={{color:'#999',margin: 5,fontSize: 15}}>Địa chỉ:</Text>
                    <Text style={{margin: 5,fontSize: 18,fontWeight:'bold'}}>{address}</Text>
                </View>
                <View style={{margin: 10,flexDirection: 'row',justifyContent:'flex-end'}}>
                    <Button title='Cập nhật' onPress={() => { 
                      navigation.navigate('updateInfoRecruiter',  {
                      id: id,
                      name: name,
                      email: email,
                      address: address,
                      phoneNumber: phoneNumber,
                    })}}/>
                </View>
              </View>
          </View>
      </ScrollView>
    </View>
      );
}
const HEADER_ICON_SIZE = 24;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
  },
  headerContainer: {
    flexDirection: 'row',
    paddingTop: 10,
    backgroundColor: '#1e88e5',
    justifyContent: 'space-between',
    paddingBottom: 12,
  },
  cartContainer: {
    paddingHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cartIcon: {
    width: HEADER_ICON_SIZE,
  },
  headerText: {
    color: '#fff',
    fontSize: 20,
    fontWeight: '500',
  },
  //
  bodyContainer: {
    flex: 1,
    backgroundColor: '#ddd',
  },
  //
  userContainer: {
    flex:1,
    backgroundColor: '#fff',
    paddingHorizontal: 30,
    paddingVertical: 22,
    alignItems: 'center',
    margin:10,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.25,
    shadowRadius: 4,
  },
  avatarContainer: {
    width: 100,
    height: 100,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black',
  },
  textContainer: {
    flex: 1,
    marginLeft: 20,
  },
  nameText: {
    color: '#1e88e5',
    fontSize: 18,
    fontWeight: '500',
  },
  gmailText: {
    color: '#828282',
  },
  divider: {
    height: 10,
  },
  itemContainer: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 10,
    alignItems: 'center',
  },
  hoso: {
    backgroundColor: '#fff',
    paddingHorizontal: 30,
    paddingVertical: 22,
    margin:10,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.25,
    shadowRadius: 4,

  },
  textinfo:{
      flex: 1,
      margin: 10,
  },
  congviecmongmuon: {
    backgroundColor: '#fff',
    paddingHorizontal: 30,
    paddingVertical: 22,
    margin: 10,
    borderRadius: 10,
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 2},
      shadowOpacity: 0.25,
      shadowRadius: 4,
  },
});
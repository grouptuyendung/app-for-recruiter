import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    ImageBackground,
    ScrollView,
    Button,
    Alert,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
export default function DetailScreen({ navigation, route }) {
    let { cardDetail } = route.params;
    let cardAdd = {
        id: cardDetail.id,
        name: cardDetail.full_name,
        school: cardDetail.type_work,
        job: cardDetail.fields,
        avatar: require('../images/avatar.png'),
        birthday: cardDetail.birthday,
        email: cardDetail.email,
        phone: cardDetail.phone,
        address: cardDetail.address,
        careerObjectives: cardDetail.years_experience,
        personalSkills: cardDetail.level_desired,
        qualifications: cardDetail.qualifications,
    }
    return (
        <View style={styles.container}>
            <ScrollView>
            <ImageBackground source={require('../images/banner.jpg')} style={{ width: '100%' }}>
                <View style={styles.header}>
                <AntDesign name="arrowleft" color="white" style={{margin: 10}} size={30} onPress={() => navigation.goBack()} />
                </View>
            </ImageBackground>
            <View style={styles.body}>
                <View style={styles.bodyContent}>
                    <View style={{ backgroundColor: '#1e88e5' }}>
                        <Image style={styles.avatar} source={cardAdd.avatar} />
                        <Text style={styles.name}>{cardAdd.name}</Text>
                        <Text style={styles.titleInfo}>Thông tin cơ bản</Text>
                    </View>
                        <View style={styles.textinfo}>
                            <Text style={styles.text}>Ngành nghề: {cardAdd.job}</Text>
                            <Text style={styles.text}>Email: {cardAdd.email}</Text>
                            <Text style={styles.text}>Số điện thoại: {cardAdd.phone}</Text>
                            <Text style={styles.text}>Ngày sinh: {cardAdd.birthday}</Text>
                            <Text style={styles.text}>Địa chỉ: {cardAdd.address}</Text>
                        </View>
                        <View style={styles.textinfo}>
                            <Text style={styles.textTitle}>Số năm kinh nghiệm</Text>
                            <Text style={styles.text}>{cardAdd.careerObjectives}</Text>
                        </View>
                        <View style={styles.textinfo}>
                            <Text style={styles.textTitle}>Cấp bậc mong muốn</Text>
                            <Text style={styles.text}>{cardAdd.personalSkills}</Text>
                        </View>
                        <View style={styles.textinfo}>
                            <Text style={styles.textTitle}>Bằng cấp chứng chỉ</Text>
                            <Text style={styles.text}>{cardAdd.qualifications}</Text>
                        </View>
                        {/* <View style={{flexDirection:'row', flex:1 ,alignItems:'flex-start', padding: 10}}>
                            <View style={{flex:1}}>
                                <Button title="Hẹn phỏng vấn" onPress={() => Alert.alert('Xác nhận', 'Bạn muốn hẹn phỏng vấn ứng viên này', [{text: 'OK', onPress: () => navigation.navigate('detailInterview', {cardDetail: cardAdd})}, {text: 'Hủy'}])}/>
                            </View>
                            <View style={{flex:1, marginRight:5, marginLeft:5}}>
                                <Button title="Quan tâm" onPress={() => Alert.alert('Thông báo', 'Đã thêm vào quan tâm ứng viên')}/>
                            </View>
                            <View style={{flex:1}}>
                                <Button title="Xem CV"/>
                            </View>
                        </View> */}
                </View>
            </View>
            </ScrollView>
        </View>
    );
}

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
    header: {
        backgroundColor: "#222",
        height: windowHeight / 4,
        opacity: 0.5,
    },
    avatar: {
        width: 130,
        height: 130,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: "white",
        marginBottom: 10,
        alignSelf: 'center',
        position: 'absolute',
        marginTop: -50
    },
    body: {

    },
    bodyContent: {
    },
    name: {
        fontSize: 28,
        color: "#eee",
        fontWeight: "600",
        marginTop: 80,
        paddingHorizontal: 90,
    },
    titleInfo: {
        fontSize: 20,
        fontWeight: 'bold',
        margin: 10,
        color: '#eee',
    },
    textinfo: {
        margin: 10,
        paddingBottom: 10,
        borderBottomWidth: 1,
    },
    text: {
        fontSize: 16,
        marginBottom: 5,
        margin: 10,
    }, textTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#1e88e5'
    }
});



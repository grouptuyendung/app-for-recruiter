import React, { useState, useEffect, useContext } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import RecruiterHomeScreen from './src/screens/recruiterHomeScreen';
import LoginScreen from './src/screens/loginScreen';
import DetailScreen from './src/screens/detailCardScreen';
import MyProfile from './src/screens/myProfile';
import nhatuyendung from './src/screens/nhatuyendung';
import SearchDetail from './src/screens/searchScreen';
import Interview from './src/screens/interviewScreen';
import detailInterview from './src/screens/detailCardInterview';
import UpdateProfile from './src/screens/updateRecruiterProfile';
import CandidatesHomeScreen from './src/screens/candidatesHomeScreen';
import SignInScreen from './src/screens/signInScreen';
import SignInScreenRecruiter from './src/screens/signInScreenRecruiter';
import SignUpScreen from './src/screens/signUpScreen';
import SignUpScreenRecruiter from './src/screens/signUpRecruiterScreen';
import AnimatedSplash from "react-native-animated-splash-screen";

import  UserContext  from './src/components/userContext';
const Stack = createStackNavigator();
function App() {

  const [isLoaded, setIsLoaded] = useState(false);
  useEffect(() => {
    setTimeout(() => {
      setIsLoaded(true);
    }, 2000);
  }, []);

  return (
    <UserContext.Provider>
    <AnimatedSplash
      translucent={false}
      isLoaded={isLoaded}
      logoImage={require("./src/assets/recruitment-icon.png")}
      backgroundColor={"#fff"}
      logoHeight={150}
      logoWidth={150}
    >
      <NavigationContainer>
        <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName="Login">

          <Stack.Screen name="SignInScreen" component={SignInScreen} />
          <Stack.Screen name="SignUpScreen" component={SignUpScreen} />
          <Stack.Screen name="SignUpScreenRecruiter" component={SignUpScreenRecruiter} />
          <Stack.Screen name="SignInScreenRecruiter" component={SignInScreenRecruiter} />
          <Stack.Screen name="Login" component={LoginScreen} />
          <Stack.Screen name="CandidatesHome" component={CandidatesHomeScreen} />
          <Stack.Screen name="MyProfile" component={MyProfile} />
          <Stack.Screen name="RecruiterHome" component={RecruiterHomeScreen} />

          <Stack.Screen name="Detail" component={DetailScreen} />

          <Stack.Screen name="nhatuyendung" component={nhatuyendung} />
          <Stack.Screen name="searchDetail" component={SearchDetail} />
          <Stack.Screen name="Interview" component={Interview} />
          <Stack.Screen name="detailInterview" component={detailInterview} />
          <Stack.Screen name="updateInfoRecruiter" component={UpdateProfile} />

        </Stack.Navigator>
      </NavigationContainer>
    </AnimatedSplash>
    </UserContext.Provider>
  );
}
export default App;